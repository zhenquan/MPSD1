//
//  ViewController.swift
//  MPSD1
//
//  Created by Sim Zhen Quan on 27/07/2021.
//

import UIKit
import AVFoundation
import Vision
import UserNotifications

class ViewController: UIViewController {
    
    var notification = Notification()
    @IBOutlet weak var label: UILabel!
    
    @IBAction func onPressScanQr(_ sender: Any) {
        let controller = UIStoryboard(name: "QrScanner", bundle: nil).instantiateViewController(withIdentifier: "QrScannerView")
        present(controller, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var myButton: UIButton!
    
    @IBAction func notiBtn(_ sender: Any) {
        notification.sendLocalNotification()
    }
}

