//
//  Notification.swift
//  Notification
//
//  Created by Sim Zhen Quan on 09/08/2021.
//

import Foundation
import UserNotifications

class Notification {
    func sendLocalNotification() {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .sound]){(granted, error) in
        }
        let content = UNMutableNotificationContent()
        content.title = "Vaccinne Appointment"
        content.body = "Your appointment is updated!"
        
        let date = Date().addingTimeInterval(5)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from:date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching:dateComponents,repeats: true)
        
        // Step 4: Create the request
        
        let uuidString = UUID().uuidString
        
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        
        //step 5: Register the request
        center.add(request) { (error) in
            //Check the error parameter and handle any errors
        }
    }
}
